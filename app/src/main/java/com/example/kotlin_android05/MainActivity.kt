package com.example.kotlin_android05

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.speech.RecognizerIntent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextClock
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val voiceRecognitionREquestCode = 1004

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupView()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if(requestCode == voiceRecognitionREquestCode && resultCode == Activity.RESULT_OK)
        {
            val matches = data!!.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
            println("Recognition results $matches")

            // 語音識別會有多個結果，第一個是最精確的
            val text = matches.first()

            val voiceTextView = findViewById(R.id.voiceText) as TextView
            voiceTextView.setText(text)
            updateTextViewWithText(text)

        }

        super.onActivityResult(requestCode, resultCode, data)


    }


    fun setupView(){
        val btn_go : Button = findViewById(R.id.recognitionButton) as Button
        btn_go.setOnClickListener(View.OnClickListener {
            startVoiceRecognitionActivity()
        })

        val tipsTextView : TextView = findViewById(R.id.tipsTextView) as TextView
        tipsTextView.setTextColor(Color.WHITE)


    }


    fun startVoiceRecognitionActivity(){
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)

        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,"Please say something")

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)

        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS,5)

        startActivityForResult(intent,voiceRecognitionREquestCode)
    }


    private fun updateTextViewWithText(text:String){
        Log.e("update",text)
        bikeTextView.setTextColor(Color.BLACK)
        climbTextView.setTextColor(Color.BLACK)
        swimTextView.setTextColor(Color.BLACK)

        tipsTextView.setTextColor(Color.WHITE)

        if(text.contains("騎車",true)){
            bikeTextView.setTextColor(Color.BLUE)
        }

        if(text.contains("爬山",true)){
            climbTextView.setTextColor(Color.BLUE)
        }

        if(text.contains("游泳",true)){
            swimTextView.setTextColor(Color.BLUE)
        }else{
            tipsTextView.setTextColor(Color.BLUE)
        }

    }




}
